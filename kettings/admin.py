from django.contrib import admin

from kettings.models import Group, Setting
from kettings.attrs import GroupAttr, SettingAttr


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    list_display = GroupAttr.admin_list_display
    list_filter = GroupAttr.admin_list_filter


@admin.register(Setting)
class SettingAdmin(admin.ModelAdmin):
    list_display = SettingAttr.admin_list_display
    list_filter = SettingAttr.admin_list_filter
    search_fields = ['key']
    list_editable = ['is_active']
