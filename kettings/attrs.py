

class GroupAttr:
    admin_list_display = ['name', 'label', 'type', 'parent', 'description', 'sort', 'is_active', 'content_type']
    admin_list_filter = ['is_active', 'type', 'parent']


class SettingAttr:
    admin_list_display = ['key', 'site', 'group', 'value', 'content_object', 'is_active']
    admin_list_filter = ['group', 'is_active']

