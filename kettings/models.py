from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from mptt.models import MPTTModel, TreeForeignKey
from mptt.managers import TreeManager


class Group(MPTTModel, models.Model):
    name = models.CharField(max_length=100)
    label = models.CharField(max_length=100, null=True, blank=True)
    type = models.CharField(max_length=50)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', on_delete=models.CASCADE)
    description = models.CharField(max_length=100, null=True, blank=True)
    sort = models.PositiveIntegerField(default=1000)
    cls_path = models.CharField(max_length=250, null=True, blank=True)
    is_active = models.BooleanField(default=False)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)

    tree = TreeManager()
    objects = models.Manager()

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('type', 'sort', 'name',)

    class MPTTMeta:
        order_insertion_by = ['sort']

    def get_label(self):
        return self.label or self.name

    def get_active_children(self):
        return self.children.filter(is_active=True)


class Setting(models.Model):
    site = models.ForeignKey('sites.Site', on_delete=models.CASCADE, related_name='%(app_label)s%(class)s_site')
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    key = models.CharField(max_length=255)
    value = models.TextField()
    is_active = models.BooleanField(default=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    sort = models.PositiveIntegerField(default=1000)

    object_id = models.PositiveIntegerField()
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    content_object = GenericForeignKey()

    def __str__(self):
        return '%s: %s' % (self.key, self.value)

    class Meta:
        ordering = ('sort', 'group', 'key',)
        unique_together = ('site', 'key', 'content_type', 'object_id', 'group')
