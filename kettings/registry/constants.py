from enum import Enum


class KettingsK(Enum):
    GRP_TYPE_VGROUP = 'VGroup'
    GRP_TYPE_VFORM = 'VForm'
