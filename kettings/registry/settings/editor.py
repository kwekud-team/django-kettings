from django.utils.module_loading import autodiscover_modules, import_string
from django.contrib.contenttypes.models import ContentType

from kettings.models import Group, Setting
from kettings.registry.constants import KettingsK
from kettings.registry.vforms import VGroup
from kettings.registry.utils import get_class_path


_RECORDS = {}


class Register:

    @staticmethod
    def add(vform_class, description=''):
        model_class = vform_class._attr.model
        key = f'{model_class.__name__}_{vform_class.__name__}'

        if key in _RECORDS:
            raise Exception(f'Setting model "{key}" already registered')

        _RECORDS[key] = {
            'model_class': model_class,
            'vform_class': vform_class,
            'group': vform_class._attr.vgroup or VGroup(),
            'description': description
        }


class SettingEditorUtils:

    def refresh_settings(self):
        autodiscover_modules('kettings')
        Group.objects.update(is_active=False)

        xs = []
        for pos, (key, record) in enumerate(_RECORDS.items()):
            model_class = record['model_class']
            vform_klass = record['vform_class']

            vform = vform_klass()

            # Create main group
            grp = record['group']()
            cls_path = get_class_path(grp)
            group = self.setup_group(KettingsK.GRP_TYPE_VGROUP.value, grp, model_class, cls_path)

            # Create sub group
            v = VGroup(
                name=vform.__class__.__name__,
                label=vform._attr.verbose_name,
                sort=pos,
                description=record['description']
            )
            cls_path = get_class_path(vform)
            self.setup_group(KettingsK.GRP_TYPE_VFORM.value, v, model_class, cls_path, parent=group)

        return xs

    def setup_group(self, ttype, group_obj, model_class, cls_path, parent=None):
        content_type = ContentType.objects.get_for_model(model_class)
        return Group.objects.update_or_create(
            name=group_obj.name,
            type=ttype,
            content_type=content_type,
            defaults={
                'description': group_obj.description,
                'sort': group_obj.sort,
                'label': group_obj.label,
                'parent': parent,
                'cls_path': cls_path,
                'is_active': True
            })[0]

    def import_form(self, vform):
        cls_path = vform.cls_path
        try:
            form = import_string(cls_path)
        except ModuleNotFoundError:
            form = None
        return form

    def get_setting(self, site, content_object, vgroup, vform, vfield_name):
        vgroup_name = get_class_path(vgroup)
        vform_name = get_class_path(vform)
        content_type = ContentType.objects.get_for_model(content_object)

        obj = Setting.objects.filter(
            site=site,
            object_id=content_object.pk,
            content_type=content_type,
            group__parent__cls_path=vgroup_name,
            group__cls_path=vform_name,
            key=vfield_name
        ).first()

        if obj:
            return obj.value
