from django.contrib.contenttypes.models import ContentType

from kettings.models import Setting
from kettings.registry.utils import get_class_path


class SettingViewerUtils:

    def __init__(self, site, content_object, vgroup_class, vform_class):
        self.site = site
        self.content_object = content_object
        self.vgroup_class = vgroup_class
        self.vform_class = vform_class

    def _get_value_qs(self, vfield_names):
        vgroup_name = (self.vgroup_class if isinstance(self.vgroup_class, str) else get_class_path(self.vgroup_class()))
        vform_name = (self.vform_class if isinstance(self.vform_class, str) else get_class_path(self.vform_class()))
        content_type = ContentType.objects.get_for_model(self.content_object)

        return Setting.objects.filter(
            site=self.site,
            object_id=self.content_object.pk,
            content_type=content_type,
            group__parent__cls_path=vgroup_name,
            group__cls_path=vform_name,
            key__in=vfield_names
        )

    def get_values(self, vfield_names):
        # qs = self._get_value_qs(vfield_names).values('key', 'value', 'group__cls_path')
        vform_obj = self.vform_class()
        vform_name = (self.vform_class if isinstance(self.vform_class, str) else get_class_path(vform_obj))

        dt = {}
        for fname, field in vform_obj.fields.items():
            if fname in vfield_names:
                value = field.get_value(self.site, self.content_object, vform_name, fname)
                dt[fname] = value

        return dt

    def get_value(self, vfield_name):
        values = self.get_values([vfield_name])
        return values.get(vfield_name, None)
