# from dataclasses import dataclass
#
#
# @dataclass
# class VObject:
#     vfield_name: str
#     vform_class: object


def get_class_path(obj):
    module = obj.__class__.__module__
    if module is None or module == str.__class__.__module__:
        return obj.__class__.__name__
    return module + '.' + obj.__class__.__name__
