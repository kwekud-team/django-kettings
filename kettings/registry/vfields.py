import os
from django.utils.module_loading import import_string
from django.contrib.contenttypes.models import ContentType
from django.conf import settings
from django.core.files.storage import default_storage
from django.core.files.uploadedfile import UploadedFile
from django import forms

from kettings.models import Setting


__all__ = (
    'VField', 'VCharField', 'VIntegerField', 'VDecimalField', 'VEmailField', 'VDateField', 'VDateTimeField',
    'VChoiceField', 'VFileField', 'VImageField'
)


class VField(forms.Field):

    def __init__(self, *args, **kwargs):
        self.vfield_default = kwargs.pop('vfield_default', None)
        self.default = kwargs.pop('default', None)

        kwargs['help_text'] = self.prep_help_text(**kwargs)

        super(VField, self).__init__(*args, **kwargs)

    def prep_help_text(self, **kwargs):
        txt = ''
        if self.default:
            txt += f'<small>Default: {self.default}</small><br />'

        txt += kwargs.pop('help_text', '')

        return txt

    def get_value(self, site, content_object, vform_name, vfield_name):
        value = self._get_value(site, content_object, vform_name, vfield_name)

        if self.vfield_default and not value:
            value = self._get_default_value(site, content_object)

        value = value or self.default

        # value = self.prepare_value(value)
        return value

    def _get_default_value(self, site, content_object):
        value = None

        if '.' in self.vfield_default:
            xs = self.vfield_default.split('.')
            vform_name = '.'.join(xs[:-1])
            fname = xs[-1]

            vform_class = import_string(vform_name)
            if vform_class:
                field = vform_class().fields.get(fname, None)
                if field:
                    value = field.get_value(site, content_object, vform_name, fname)

        return value

    def _get_value(self, site, content_object, vform_name, vfield_name):
        if content_object:

            content_type = ContentType.objects.get_for_model(content_object)

            obj = Setting.objects.filter(
                site=site,
                object_id=content_object.pk,
                content_type=content_type,
                # group__parent__cls_path=vgroup_name,
                group__cls_path=vform_name,
                key=vfield_name
            ).first()
            if obj:
                return obj.value

    def pre_save(self, field_name, value, form, vform):
        return value

    def post_save(self, field_name, value, form, vform, setting_obj):
        pass


class VCharField(forms.CharField, VField):
    pass


class VIntegerField(forms.IntegerField, VField):
    pass


class VDecimalField(forms.DecimalField, VField):
    pass


class VEmailField(forms.EmailField, VField):
    pass


class VDateField(forms.DateField, VField):
    pass


class VDateTimeField(forms.DateTimeField, VField):
    pass


class VChoiceField(forms.ChoiceField, VField):
    pass


class VFileField(forms.FileField, VField):
    upload_to = 'kettings/'

    def prepare_value(self, value):

        if value:
            class Obj:
                def __init__(self, url):
                    self.raw_url = url
                    self.url = f'{settings.MEDIA_URL}{url}'

                def __str__(self):
                    return str(self.raw_url).split('/')[-1]

            return Obj(value)

        return value

    def post_save(self, field_name, value, form, vform, setting):
        if isinstance(value, UploadedFile):
            path = f'{self.upload_to}{setting.site.pk}/{setting.content_type.pk}-{setting.object_id}/{field_name}' \
                f'/{value.name}'
            actual_path = os.path.join(settings.MEDIA_ROOT, path)

            default_storage.save(actual_path, value)

            setting.value = path
            setting.save(update_fields=['value'])


class VImageField(forms.ImageField, VFileField):
    pass

    # def prepare_value(self, value):
    #     return value
        # return f'{settings.MEDIA_URL}{value}'

# class MultipleChoiceVField(forge.MultipleChoiceField, ChoiceVField):
#     description = 'MultipleChoice'
#     append_empty = False
#
#     def clean(self, value):
#         val = super(MultipleChoiceVField, self).clean(value)
#         val_str = ','.join(val)
#         return val_str
#
#     def get_initial_value(self):
#         val = self._value
#         return (val.split(',') if val else [])
#
#     def get_display_value(self, value=None):
#         value = super(ChoiceVField, self).get_display_value(value)
#         xs = []
#         if value:
#             value_xs = value.split(',')
#             for x in value_xs:
#                 tmp = dict(self.choices).get(x, None) or DEFAULT_VALUE
#                 if tmp:
#                     xs.append(tmp)
#
#         return ', '.join(xs)
#
#
# class MultiFieldVField(forge.CharField, VField):
#     description = 'MultiField'
#
#
# class MultiFieldVField_(forge.MultiVFieldField, VField):
#     description = 'MultiField'
#
#     def __init__(self, *args, **kwargs):
#         fields = []  # self.get_multi_fields()
#         kwargs = super(MultiFieldVField, self).__init__(fields=fields, *args, **kwargs)
#         self.fields = self.get_multi_fields()
#
#     def get_widget_class(self, widget_path):
#         self.multi_fields = self.get_multi_fields()
#         widget_cls = MultiWidget(self.multi_fields)
#         return widget_cls
#
#     def get_multi_fields(self):
#         xs = self._extra_kwargs.get('multi_fields', [])
#         fields = []
#         for x in xs:
#             f_path = '%s.values.base.%s' % (SURVEY_MODULE_PATH, x['input_type'])
#             f_cls = my_import(f_path)
#             f_obj = f_cls(kwargs_from_db=False,
#                           extra_kwargs=x.get('extra_kwargs', {}),
#                           **x.get('form_kwargs', {})
#                           )
#             fields.append(f_obj)
#         return fields
#
#     def compress(self, data_list):
#         val_str = ','.join(data_list)
#         return val_str
#
#     def get_display_value(self, value=None):
#         value = super(MultiFieldVField, self).get_display_value(value)
#         xs = []
#         if value:
#             value_xs = value.split(',')
#             for pos, f in enumerate(self.fields):
#                 try:
#                     index = value_xs[pos]
#                 except:
#                     index = None
#                 if index:
#                     tmp = f.get_display_value(value_xs[pos])
#                     if tmp:
#                         xs.append(tmp)
#
#         return ' | '.join(xs)


# class FormsetVField(forge.CharField, VField):
#     description = 'Formset'
#
#     def get_widget_class(self, widget_path):
#         widget_class = super(FormsetVField, self).get_widget_class(widget_path)
#         widget_cls = widget_class(self.get_expected_questions(), self.capture, can_change=self.get_can_change())
#         return widget_cls
#
#     def get_fields(self):
#         formset_source = self._extra_kwargs.get('formset_source', None)
#         formset_value = self._extra_kwargs.get('formset_value', {})
#
#         fields = []
#         if formset_source == 'QUERYSET':
#             from components.survey.models import Question
#             qs = Question.objects.filter(question_type=QUESTION_TYPES['QUESTION_CHOICE'])
#             qs = qs.filter(**formset_value)
#
#             for obj in qs:
#                 f_path = '%s.values.base.%s' % (SURVEY_MODULE_PATH, obj.input_type)
#                 f_cls = my_import(f_path)
#
#                 x = obj.get_extra_kwargs()
#                 f_obj = f_cls(kwargs_from_db=False,
#                               extra_kwargs=x.get('extra_kwargs', {}),
#                               **x.get('form_kwargs', {})
#                               )
#                 fields.append(f_obj)
#         return fields
#
#     def get_expected_questions(self):
#         formset_source = self._extra_kwargs.get('formset_source', None)
#         formset_value = self._extra_kwargs.get('formset_value', {})
#
#         from components.survey.models import Question
#         qs = Question.objects.none()
#
#         if formset_source == 'QUERYSET':
#             qs = Question.objects.filter(parent=self.question, is_active=True,
#                                          question_type=QUESTION_TYPES['QUESTION_CHOICE'])
#             qs = qs.filter(**formset_value)
#
#         return qs
#
#     def get_initial_value(self):
#         value = super(FormsetVField, self).get_initial_value()
#         val = None
#
#         json_val = load_json_or_none(value)
#         if not json_val and self.question:
#             init_val = self.question.initial_value
#             json_val = load_json_or_none(init_val)
#
#         if json_val:
#             val = []
#             for row in json_val:
#                 tmp = {}
#                 for obj in self.get_expected_questions():
#                     key = 'field__%s' % obj.pk
#                     v = row.get(obj.slug, "") or row.get(key, "")
#                     tmp[key] = v
#                 val.append(tmp)
#
#         return val
#
#     def clean(self, value):
#         val_str = json.dumps(value)
#         return val_str
#
#     def get_can_change(self):
#         return self._extra_kwargs.get('formset_can_change', True)
#
#     def get_display_value(self, value=None):
#         value = super(FormsetVField, self).get_display_value(value)
#         html = DEFAULT_VALUE
#
#         json_value = load_json_or_none(value)
#         if json_value:
#             pks = [x.replace('field__', '') for x in json_value[0]]
#             pks = (x for x in pks if intify(x))
#             header_xs = Question.objects.filter(pk__in=pks, is_active=True)
#
#             body_xs = []
#             for row in json_value:
#                 tmp = []
#                 for x in header_xs:
#                     tmp.append({
#                         'value': row['field__%s' % x.pk],
#                         'extra': x.extra
#                     })
#                 if tmp:
#                     body_xs.append(tmp)
#
#             if header_xs and body_xs:
#                 html = render_to_string('survey/values/formset_display.html',
#                                         {'headers': header_xs, 'content': body_xs})
#
#         return mark_safe(html)
