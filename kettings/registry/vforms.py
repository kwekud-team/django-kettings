from django.db import models
from django import forms
from django.forms.forms import DeclarativeFieldsMetaclass
from kettings.registry.utils import get_class_path

from kettings.registry.settings.viewer import SettingViewerUtils


class VGroup:
    model_class: models.Model
    name: str = '---'
    label: str = ''
    description: str = ''
    cls_path: str = ''
    sort: int = 1000

    def __init__(self, **kwargs):
        fields = ['name', 'label', 'description', 'cls_path', 'sort']
        for f in fields:
            if f in kwargs:
                setattr(self, f, kwargs[f])

    @classmethod
    def init_viewer(cls, site, content_object, vform_class):
        return SettingViewerUtils(site, content_object, cls, vform_class)


class VFormMetaClass(DeclarativeFieldsMetaclass):
    """
        We want to be able to get the literal string representation of fields declared in form. We need to be able
        to just do something like Class.K.field_name and we will get 'field_name'. We want to prevent hard-coding
        field names and let already defined fields act as constants
    """
    def __new__(mcs, name, bases, attrs):
        new_class = super(VFormMetaClass, mcs).__new__(mcs, name, bases, attrs)

        class K:
            # Sub class to hold field names as string
            pass

        class DK:
            # Sub class to hold module path of fields as string
            pass

        new_class.K = K
        new_class.DK = DK

        for key, value in list(attrs['declared_fields'].items()):
            setattr(new_class.K, key, key)

            nm = f'{get_class_path(new_class())}.{key}'
            setattr(new_class.DK, key, nm)

        new_class._attr = new_class.Attr()

        return new_class


class VForm(forms.Form, metaclass=VFormMetaClass):

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        self.content_object = kwargs.pop('content_object', None)
        super(VForm, self).__init__(*args, **kwargs)

        # self._attr = self.Attr()
        self.setup_attrs()

    class Attr:
        # Attr is similar to Meta subclass on Form. We don't want to mess with Meta class so we use this to declare
        # extra attributes not supported by django forms
        model = None

    @classmethod
    def init_viewer(cls, site, content_object):
        return SettingViewerUtils(site, content_object, cls._attr.vgroup, cls)

    def setup_attrs(self):
        self._attr.fieldsets = self.get_fieldsets()
        self._attr.verbose_name = self.get_verbose_name()

    def get_attr_value(self, attr_name, default=None):
        return getattr(self._attr, attr_name, default)

    def get_verbose_name(self):
        name = self.get_attr_value('verbose_name')
        if not name:
            klass_name = self.__class__.__name__
            name = klass_name

        return name

    def get_fieldsets(self):
        fieldsets = self.get_attr_value('fieldsets', [])
        if not fieldsets:
            fieldsets = [
                (None, {'fields': list(self.declared_fields.keys())}),
            ]

        return fieldsets
