from django import forms
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe

# from kettings.registry.forge import ValueForm
# from components.survey.utils import get_form_values


class MultiWidget(forms.widgets.MultiWidget):
    def __init__(self, xfields, *args, **kwargs):
        self.xfields = xfields
        xwidgets = []

        for x in xfields:
            widget = x.widget
            widget_attrs = x._extra_kwargs.get('widget_attrs', {})
            if widget_attrs:
                widget.attrs = widget_attrs

            xwidgets.append(widget)
        super(MultiWidget, self).__init__(tuple(xwidgets), *args, **kwargs)

    def decompress(self, value):
        xs = []
        if value:
            xs = value.split(',')
        return xs


class FormsetWidget(forms.widgets.Widget):
    def __init__(self, expected_questions, capture, *args, **kwargs):
        self.expected_questions = expected_questions
        self.capture = capture
        self.can_change = kwargs.pop('can_change', False)
        super(FormsetWidget, self).__init__(*args, **kwargs)

    class Media(forms.widgets.Media):
        js = (
            'website/local/js/jquery.formset.js',
        )
        css = {
            'all': ('website/local/css/jquery.formset.css',)
        }

    def render(self, name, value, renderer, attrs={}):
        values = get_form_values(self.expected_questions, self.capture)

        class GenFormMetaClass(ValueForm):
            def __new__(cls, *args, **kwargs):
                kwargs['values'] = values
                return ValueForm(*args, **kwargs)

        GenFormset = forms.formset_factory(GenFormMetaClass, extra=1)
        data = self.prepare_data(name, value)
        formset = GenFormset(data, prefix=name)

        input_str = render_to_string('survey/values/formset_tabular.html',
                                     {'formset': formset, 'name': name,
                                      'can_change': self.can_change}
                                     )
        return mark_safe(input_str)

    def value_from_datadict(self, data, files, name):
        total_forms = data.get('%s-TOTAL_FORMS' % name, 0)
        xs = []
        for x in range(int(total_forms)):
            tmp = {}
            for k, v in data.items():
                key = '%s-%s-' % (name, x)
                if k.startswith(key):
                    val_k = k.replace(key, '')
                    tmp[val_k] = v
            xs.append(tmp)

        return xs

    def prepare_data(self, name, value):
        data = None
        if value:
            data = {}
            data['%s-TOTAL_FORMS' % name] = len(value)
            data['%s-INITIAL_FORMS' % name] = '0'
            data['%s-MAX_NUM_FORMS' % name] = '1000'

            for pos, row in enumerate(value):
                for k, v in row.items():
                    key = '%s-%s-%s' % (name, pos, k)
                    data[key] = v

        return data