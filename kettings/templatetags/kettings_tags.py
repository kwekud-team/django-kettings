from django import template
from django.utils.module_loading import import_string

register = template.Library()


@register.simple_tag
def get_settings(site, content_object, module_path, fields=None):
    xs = module_path.split('.')
    import_path = f'{".".join(xs[:-1])}.kettings.{xs[-1]}'

    vform_class = import_string(import_path)

    if fields:
        fields = fields.split(',')
    else:
        fields = list(vform_class().fields.keys())

    sett_viewer = vform_class.init_viewer(site, content_object)
    return sett_viewer.get_values(fields)
