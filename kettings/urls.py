from django.urls import path

from kettings import views

app_name = 'kettings'


urlpatterns = [
    path('editor/st-<int:site_pk>/ct-<int:content_type_pk>/oi-<int:object_pk>/',
         views.SettingsEditorView.as_view(), name='settings_editor'),

    path('editor/st-<int:site_pk>/ct-<int:content_type_pk>/oi-<int:object_pk>/',
         views.SettingsEditorView.as_view(), name='settings_editor'),
    path('manager/st-<int:site_pk>/ct-<int:content_type_pk>/oi-<int:object_pk>/cf-<int:vform_pk>/',
         views.SettingsEditorView.as_view(), name='settings_editor'),

    path('viewer/st-<int:site_pk>/ct-<int:content_type_pk>/oi-<int:object_pk>/',
         views.SettingsViewerView.as_view(), name='settings_viewer'),
    path('viewer/st-<int:site_pk>/ct-<int:content_type_pk>/oi-<int:object_pk>/cf-<int:vform_pk>/',
         views.SettingsViewerView.as_view(), name='settings_viewer'),
]
