from django.contrib.sites.models import Site
from django.contrib.contenttypes.models import ContentType
from django.contrib import messages
from django.views.generic import TemplateView
from django.views.generic.edit import FormView
from django.shortcuts import get_object_or_404
from django import forms

from kettings.registry.settings.editor import SettingEditorUtils
from kettings.registry.settings.viewer import SettingViewerUtils
from kettings.registry.constants import KettingsK
from kettings.models import Group, Setting


class SettingsBase(TemplateView):
    site = None
    vform = None
    content_object = None
    ctn_type = None  # TemplateView already has attr named 'content_type'
    object = None

    def dispatch(self, request, *args, **kwargs):
        SettingEditorUtils().refresh_settings()

        self.site = get_object_or_404(Site, pk=self.kwargs['site_pk'])
        self.ctn_type = get_object_or_404(ContentType, pk=self.kwargs['content_type_pk'])
        self.vform = self.get_vform()
        self.content_object = self.get_content_object()
        return super(SettingsBase, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(SettingsBase, self).get_context_data(**kwargs)

        context.update({
            'vgroups': self.get_vgroups(),
            'vform': self.vform,
            'site': self.site,
            'content_type': self.ctn_type,
            'object_pk': self.kwargs['object_pk']
        })

        return context

    def get_vgroups(self):
        return Group.objects.filter(is_active=True, type=KettingsK.GRP_TYPE_VGROUP.value, content_type=self.ctn_type)

    def get_vform(self):
        all_vforms = Group.objects.filter(is_active=True, type=KettingsK.GRP_TYPE_VFORM.value)
        vform_pk = self.kwargs.get('vform_pk', None)
        if vform_pk:
            all_vforms = all_vforms.filter(pk=vform_pk)

        return all_vforms.first()

    def get_content_object(self):
        return get_object_or_404(self.ctn_type.model_class(), pk=self.kwargs['object_pk'])


class SettingsEditorView(FormView, SettingsBase):
    template_name = 'kettings/settings_editor.html'

    def get_form_kwargs(self):
        kw = super().get_form_kwargs()
        kw['request'] = self.request
        kw['content_object'] = self.content_object
        return kw

    def get_initial(self):
        initial = super(SettingsEditorView, self).get_initial()

        qs = Setting.objects.filter(
            site=self.site,
            group=self.vform
        ).values('key', 'value')

        dt = {x['key']: x['value'] for x in qs}
        initial.update(dt)

        return initial

    def form_valid(self, form):
        if self.vform:

            for key, val in form.cleaned_data.items():
                vfield = form.fields[key]

                val = vfield.pre_save(key, val, form, self.vform)
                val = val or ''

                setting = Setting.objects.update_or_create(
                    site=self.site,
                    group=self.vform,
                    key=key,
                    object_id=self.kwargs['object_pk'],
                    content_type=self.ctn_type,
                    defaults={
                        'value': val,
                    })[0]

                vfield.post_save(key, val, form, self.vform, setting)

            messages.success(self.request, 'Updated settings successfully')
            self.success_url = self.request.get_full_path()

        return super(SettingsEditorView, self).form_valid(form)

    def get_form_class(self):
        form_class = None

        if self.vform:
            form_class = SettingEditorUtils().import_form(self.vform)

        if not form_class:
            class DummyForm(forms.Form):
                pass
            form_class = DummyForm

        return form_class


class SettingsViewerView(SettingsBase):
    template_name = 'kettings/settings_viewer.html'

    def get_settings_values(self):
        set_viewer = SettingViewerUtils(self.site, self.content_object)

        return set_viewer.get_values(self.vform.parent.cls_path, self.vform.cls_path)
        # res1 = set_viewer.get_value(self.vform.parent.cls_path, self.vform.cls_path, 'app_name')

    def get_context_data(self, **kwargs):
        context = super(SettingsViewerView, self).get_context_data(**kwargs)

        context.update({
            'values': self.get_settings_values(),
        })

        return context
